#include <iostream>
#include <string>
using namespace std;

class Score
{
private:
	double math;
	double english;
	double chinese;
public:
	void GetScore()
	{
		cout << "请输入数学成绩:";
		cin >> this->math;
		cout << "请输入英语成绩:";
		cin >> this->english;
		cout << "请输入语文成绩:";
		cin >> this->chinese;
	}
	friend class Student;
};

class Student
{
private:
	string id;
	string name;
	Score score;
public:
	Student(string id, string name, Score score)
	{
		this->id = id;
		this->name = name;
		this->score = score;
	}
	Student()
	{

	}
	void GetInfo()
	{
		cout << "请输入学生学号:";
		cin >> this->id;
		cout << "请输入学生姓名:";
		cin >> this->name;
		this->score.GetScore();
	}
	void PrintInfo()
	{
		cout << this->id << "\t"
			 << this->name << "\t"
			 << this->score.math << "\t"
			 << this->score.english << "\t"
			 << this->score.chinese << endl;
	}
	
};


int main()
{
	Student student;
	cout << "请输入学生信息:" << endl;
	student.GetInfo();
	student.PrintInfo();


	return 0;
}