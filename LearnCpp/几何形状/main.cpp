#include <iostream>
using namespace std;

#define PI 3.14  

// 形状 类     接口
class Shape
{
public:
	// 类里面定义了一个方法，理论上来说都要写方法体的
	virtual double GetArea() = NULL;  // 抽象方法   
};

// 正方形类     A is B    鸟 是 动物
class Square:public Shape    // Reactangle 
{
public:
	Square(double b)
	{
		this->边长 = b;
	}
	double GetArea()
	{
		return  边长 * 边长;
	}
private:
	double 边长;
};

// 圆形
class Circle:public Shape
{
public:
	Circle(double r)//构造函数然后重载
	{
		this->radius = r;
	}
	double GetArea()
	{
		return  PI * radius * radius;
	}
private:
	double radius;
};

// 打印一个形状的面积，但是不需要关心这个形状具体是什么形状
void PrintArea(Shape*s)
{
	cout << s->GetArea() << endl;
}


int main()
{

	// 父类指针指向子类对象    多态

	Square s(3);//给构造函数传实参；
	Circle c(3);

	PrintArea(&s);
	PrintArea(&c);

	return 0;
}
