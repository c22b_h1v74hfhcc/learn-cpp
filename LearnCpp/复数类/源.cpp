#include <iostream>
#include <string>
using namespace std;

class Complex
{
private:
	double real;
	double imag;
public:
	Complex()
	{

	}
	Complex(double real, double imag)
	{
		this->real = real;
		this->imag = imag;
	}
	void SetReal(double real)
	{
		this->real = real;
	}
	void SetImag(double imag)
	{
		this->imag = imag;
	}
	double GetReal()
	{
		return this->real;
	}
	double GetImag()
	{
		return this->imag;
	}
	void Print()
	{
		cout << this->real;
		if (this->imag > 0)
		{
			cout << "+";
		}
		cout << this->imag << "i";
	}
	Complex& operator+(const Complex& c)
	{
		Complex result;
		result.real = this->real + c.real;
		result.imag = this->imag + c.imag;
		return result;
	}
	Complex& operator-(const Complex& c)
	{
		Complex result;
		result.real = this->real - c.real;
		result.imag = this->imag - c.imag;
		return result;
	}
	friend ostream& operator<<(ostream& out, const Complex& c)
	{
		out << c.real;
		if (c.imag > 0)
		{
			out << "+";
		}
		out << c.imag << "i";
		return out;
	}
	friend istream& operator>>(istream& in, Complex& s)
	{
		// 键盘输入 3 4 要变成  Complex 对象  s
		// in 当作 cin 用
		in >> s.real;
		in >> s.imag;
		return in;
	}
};

int main()
{
	Complex a(3, 4);
	Complex b(5, 6);

	cout << "输入第一个复数:";
	cin >> a;
	cout << "输入第二个复数:";
	cin >> b;
	Complex c = a + b;
	cout << "相加的结果是:";
	cout << c;


	return 0;
}