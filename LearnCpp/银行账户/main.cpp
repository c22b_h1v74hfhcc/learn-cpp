#include <iostream>
#include <cstring>
using namespace std;

class Account
{
public:
	Account(char name[], long num, float amount);	//类的有参构造函数
	Account();						//类的无参构造函数
	void deposit(float amount); 	//往账户中存款
	int withdraw(float amount); 	//从账户中取款
	float getBalance();  			//查询余额
private:
	char mName[20];      			//银行账户的户名
	long mSN;         				//本账户的帐号
	float mBalance;   				//本账户当前的余额
};

Account::Account()
{
	strcpy(this->mName, "");
	this->mSN = 0;
	this->mBalance = 0;
}

Account::Account(char name[], long num, float amount)
{
	strcpy(this->mName, name);
	this->mSN = num;
	this->mBalance = amount;
}

void Account::deposit(float amount)
{
	this->mBalance += amount;
}

int Account::withdraw(float amount)
{
	if (mBalance >= amount)
	{
		this->mBalance -= amount;
		return 1;
	}
	else
	{
		return 0;
	}
	
}

float Account::getBalance()
{
	return this->mBalance;
}


int main()
{
	Account account(new char[] {"asd"}, 123, 100.0f);

	cout << "当前余额:" << account.getBalance() << endl;
	cout << "存200.0元" << endl;
	account.deposit(200.0F);
	cout << "当前余额:" << account.getBalance() << endl;
	cout << "取55.0元" << endl;
	account.withdraw(55.0F);
	cout << "当前余额:" << account.getBalance() << endl;


	return 0;
}