#include <iostream>
using namespace std;

class Node
{

};

class SingleList
{
public:
	~SingleList()
	{
		cout << "调用了析构函数" << endl;
		// 删除链表所有节点的代码，以防止一直占用程序的内存空间，导致电脑卡顿
	}
	void deleteAll()
	{

	}

private:
	Node* head; // 头节点
};

int main()
{
	SingleList *pList = new SingleList();

	// pList->deleteAll();
	delete pList;

	return 0;
}