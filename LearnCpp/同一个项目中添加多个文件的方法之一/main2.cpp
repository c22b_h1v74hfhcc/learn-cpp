//在构造函数（不带参数）体内部对 对象中的数据成员进行初始化
#include <iostream>
using namespace std;
class Time
{public:
    Time()  //构造函数的名字必须与类名相同
    {
        hour = 0;//用构造函数 对 对象中的数据成员进行初始化
        minute = 0;
        sec = 0;
    }
    void set_time();
    void show_time();
private:
    int hour;
    int minute;
    int sec;
};
void Time::set_time()
{
    cout << "请输入你的时间为：" << endl;
    cin >> hour;
    cin >> minute;
    cin >> sec;

}
void Time::show_time()
{
    cout << hour << "：" <<minute<<":"<<sec<< endl;
}


int main()
{
    Time t1;
    t1.set_time();
    cout << "t1所显示的时间为：\t";
    t1.show_time();
	return 0;
}
