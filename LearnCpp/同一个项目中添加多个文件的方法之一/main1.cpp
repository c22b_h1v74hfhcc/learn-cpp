#include <iostream>
#define MaxSize 50
using namespace std;
typedef struct {
	char data[MaxSize];
	int length;
}SqList;
class List
{
private:
	SqList* L;//顺序表的指针
public:
	List();//无参构造函数 ，初始化顺序表
	List(char a[], int n);//用字符数组a的n个元素值构造顺序表，假设n为a的元素个数 
	~List(); //析构函数，释放顺序表的指针 
	bool Empty();//判断顺序表是否为空，是空就返回true，否则返回false
	int ListLength();//返回顺序表的长度
	void PrintList();//输出顺序表
	char GetElem(int i);//返回第i个元素的指，i为序号，i大于等于1且小于等于length
	int LocateElem(char e);//查找顺序表中元素值为e的序号并返回序号值，如果找不到就返回0
	bool ListInsert(int i, char e);//在序号i位置插入新元素e。插入成功返回true，否则返回false
	bool ListDelete(int i, char& del_val);//删除顺序表序号为i的元素，并把删除元素的值放在del_val中。删除成功返回true，否则返回false	 
};
List::List()
{
	//初始化顺序表，并赋初始值，建立一个空链表
	this->L = new SqList;//new找到Sqlist的内存并返回指针L
	this->L->length = 0;
}
List::List(char a[], int n)
{ 
}
List::~List()
{
	delete this->L;
}

