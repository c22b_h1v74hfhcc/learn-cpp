#include <iostream>
using namespace std;

class Father
{
private:
	int a;
protected:
	int b;
public:
	int c;
};

class Son1:public Father
{
public:
	void test()
	{
		//cout << this->a;   // 公有继承时，子类不能访问父类 的私有属性
		cout << this->b;   // 公有继承时，子类可以访问父类 的保护属性
		cout << this->c;   // 公有继承时，子类可以访问父类 的公有属性
	}
};


class Son2 :protected Father
{
public:
	void test()
	{
		// cout << this->a;   // 保护继承时，子类不能访问父类 的私有属性
		cout << this->b;   // 保护继承时，子类可以访问父类 的保护属性
		cout << this->c;   // 保护继承时，子类可以访问父类 的公有属性
	}
};

class Son3 :private Father
{
public:
	void test()
	{
		// cout << this->a;   // 私有继承时，子类不能访问父类 的私有属性
		cout << this->b;   // 私有继承时，子类可以访问父类 的保护属性
		cout << this->c;   // 私有继承时，子类可以访问父类 的公有属性
	}
};

class Son2Son :public Son2
{
public:
	void test()
	{
		cout << this->c; // c不是私有，c是保护的
	}
};

class Son3Son :public Son3
{
public:
	void test()
	{
		//cout << this->c; // 不是保护，不是公有，那就是私有
	}
};

int main()
{
	Father f;

	//cout << f.a;   // 在类外不能访问保护属性

	Son1 s1;
	Son2 s2;
	Son3 s3;
	Son2Son s2s;

	cout << s1.c; 
	// cout << s2.c; // 保护继承之后，公有的属性变成了 ？（可能是私有的、也可能是保护的）   经验证，是保护属性
	// cout << s3.c; //私有继承之后，公有的属性变成了 ？（可能是私有的、也可能是保护的）     经验证，是私有属性

	//cout << s2s.c //  Son2Son 的 c 不是公有

	return 0;
}