#include<iostream>
using namespace std;
class Cuboid
{
private:
	int height;
	int length;
	int width;
public:
	// 由键盘分别输入个长方体的长、宽、高
	void Input()
	{
		cout << "输入长方体的长:";
		cin >> this->length;
		cout << "输入长方体的宽:";
		cin >> this->width;
		cout << "输入长方体的高:";
		cin >> this->height;
	}
	// 计算长方体的体积
	int GetVolume()
	{
		return length * width * height;
	}
	// 输出个长方体的体积
	void PrintVolume()
	{
		cout << this->GetVolume();
	}
};


// Ctrl + K D  让代码变得整齐
int main()
{
	Cuboid cubes[3];

	for (int i = 0; i < 3; i++)
	{
		cout << "请输入第" << i + 1 << "个长方体的长宽高" << endl;
		cubes[i].Input();
		cout << endl;
	}

	for (int i = 0; i < 3; i++)
	{
		cout << "第" << i + 1 << "个长方体的体积是";
		cubes[i].PrintVolume();
		cout << endl;
	}


	return 0;
}


